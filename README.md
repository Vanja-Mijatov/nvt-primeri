# Napredne veb tehnologije - Primeri

## Opis

Ovaj repozitorijum sadrži primere za vežbe iz predmeta Napredne veb tehnologije (Softversko inženjerstvo i informacione tehnologije).


## Struktura repozitorijuma

| # | Direktorijum | Opis |
| --- | --- | --- |
| 1 | Example 1 | Primer aplikacija koje komuniciraju putem MQTT i AMQP protokola |
| 2 | Example 2 | Primer aplikacije koja periodično generiše vrednosti i čuva ih u InfluxDB bazi podataka, a kasnije ih čita iz iste i šalje klijentu putem WebSocket protokola | 
| 3 | Example 3 | Primer testiranja performansi aplikacije pomoću Locust-a | 
| 4 | Example 4 | Primer Angular aplikacije koja demonstrira efikasnu prezentaciju podataka | 
| 5 | Example 5 | Primer podešavanja Nginx-a | 
| 6 | ISA-E2 | Materijali za vežbe iz predmeta Internet softverske arhitekture (E2); Direktorijum sadrži veći broj primera koji mogu biti korisni prilikom izrade rešenja predmetnog projekta |
