# Example of a simple UI design

This example demonstrates various techniques and mechanisms for UI design when presenting data.

## Requirements

* Node.js
* Angular 

## Setup

1. Install Node.js and Angular CLI
2. Open terminal in *app-example* directory and run following command to install required packages:
```console
npm install
```
3. Using Python from virtual environemnt run
```console
npm start
```
4. Open browser and navigate to *http://localhost:4200*