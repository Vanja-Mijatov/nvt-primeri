INSERT INTO cars (id, make, model, year, price) VALUES (1, 'Toyota', 'Camry', 2020, 24000);
INSERT INTO cars (id, make, model, year, price) VALUES (2, 'Honda', 'Civic', 2019, 22000);
INSERT INTO cars (id, make, model, year, price) VALUES (3, 'Ford', 'Mustang', 2021, 35000);
INSERT INTO cars (id, make, model, year, price) VALUES (4, 'Tesla', 'Model S', 2022, 79999);
INSERT INTO cars (id, make, model, year, price) VALUES (5, 'Chevrolet', 'Impala', 2018, 27000);
