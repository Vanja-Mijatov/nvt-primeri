INSERT INTO products (id, name, description, price) VALUES (1, 'Laptop', 'A high-performance laptop', 1200);
INSERT INTO products (id, name, description, price) VALUES (2, 'Smartphone', 'Latest Android smartphone', 800);
INSERT INTO products (id, name, description, price) VALUES (3, 'Headphones', 'Noise-cancelling headphones', 200);
INSERT INTO products (id, name, description, price) VALUES (4, 'Keyboard', 'Mechanical keyboard with RGB lighting', 150);
INSERT INTO products (id, name, description, price) VALUES (5, 'Monitor', '4K Ultra HD Monitor', 300);