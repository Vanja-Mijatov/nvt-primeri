# ISA-E2 Primeri sa vežbi

## Opis

Ovaj direktorijum sadrži primere za vežbe iz predmeta Internet softverska arhitektura za usmerenje Računarstvo i automatika.


## Struktura repozitorijuma

| # | Direktorijum | Opis |
| --- | --- | --- |
| 1 | Vezbe01 | Primer inicijalizacije Spring Boot aplikacije |
| 2 | vezbe02 | Primer Spring Boot aplikacije sa REST API-em | 
| 3 | vezbe03 | Primer aspekata, asnihronog procesiranja i zakazivanja zadataka u Spring Boot aplikaciji | 
| 4 | vezbe04 | Primer ORM i rada sa bazom podataka u Spring Boot aplikaciji |
| 5 | vezbe05 | Primer konfiguracije bezbednosnih mehanizama u Spring Boot aplikaciji |
| 6 | vezbe06 | Primer keširanja i rate limiting-a u Spring Boot aplikaciji |
| 7 | vezbe07 | Primer transakcija u Spring Boot aplikaciji |
| 8 | vezbe08 | Primer rada sa redovima poruka u Spring Boot aplikaciji |
| 9 | vezbe09 | Primer rada sa WebSocket protokolom u Spring Boot aplikaciji |