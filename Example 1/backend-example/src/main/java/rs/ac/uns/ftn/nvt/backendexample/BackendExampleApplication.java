package rs.ac.uns.ftn.nvt.backendexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackendExampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(BackendExampleApplication.class, args);
    }

}
